# spadedoc

`spadedoc` generates HTML documentation for Spade code.

## Install

Clone the repository and then run:

```bash
cd spadedoc
git submodule update --init --recursive
cargo install --path .
```

## Usage

Use `spadedoc --help` for usage information.
